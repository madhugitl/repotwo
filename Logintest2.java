package com.kardia.dev.testcases;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.kardia.dev.base.TestBase;
import com.karida.dev.pages.KLoginPage;

public class Logintest extends TestBase{
	public static KLoginPage klp;
	public Logintest() {
		super();
	}
	
	@BeforeMethod
	public void setUp() {
		initialize();
		 klp = new KLoginPage();	
		
	}
	
	@Test
	public void logTest() {
		klp.login(prop.getProperty("username"), prop.getProperty("pwd"));
		
	}
}
